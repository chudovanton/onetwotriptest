//
//  O2T_Matrix.m
//  OneTwoTripMatrix
//
//  Created by Chudov Anton on 06/10/2016.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import "O2T_Matrix.h"

@implementation O2T_Matrix{
    NSMutableArray <NSMutableArray<NSNumber *>*>* matrix;
    int matrixSize;
    int matrixCenter;
}


-(instancetype) init2nMinus1Matrix:(int) n{
    matrixSize = 2*n - 1;
    matrixCenter = n-1;
    
    matrix = [NSMutableArray arrayWithCapacity:matrixSize];
    
    for(int i=0;i<matrixSize;i++){
        NSMutableArray *arrayLine = [NSMutableArray arrayWithCapacity:matrixSize];
        
        for(int j = 0;j<matrixSize;j++){
            arrayLine[j] = [NSNumber numberWithInt:arc4random_uniform(10)];
            
        }
        matrix[i] = arrayLine;
    }
    return self;
}

-(NSNumber*) matrixValueForIdx1: (int) idx1 Idx2:(int) idx2{
    NSArray  *line = matrix[idx1];
    return  line[idx2];
}

-(int) matrixSize{
    return matrixSize;
}

-(NSArray<NSNumber *>*) getSpiralSequence{
    
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:matrixSize*matrixSize];
    
    int i     = matrixCenter;
    int j     = matrixCenter;
    
    int d_i   = 0;
    int d_j   = -1;
    
    int max_i = matrixCenter;
    int min_i = matrixCenter;
    int max_j = matrixCenter;
    int min_j = matrixCenter;
    int k = 1;
    
    
    result[0] = [self matrixValueForIdx1:matrixCenter Idx2:matrixCenter];
    
    while(k< matrixSize * matrixSize){
        i += d_i;
        j += d_j;
        
        if(i > matrixSize || i < 0 || j > matrixSize || j < 0){
            return result;
        }
        
        result[k] = [self matrixValueForIdx1:i Idx2:j];
        
        bool doTurn = false;
        if(i > max_i){
            max_i = i;
            doTurn = true;
        }
        if(i < min_i){
            min_i = i;
            doTurn = true;
        }
        if(j > max_j){
            max_j = j;
            doTurn = true;
        }
        if(j < min_j){
            min_j = j;
            doTurn = true;
        }
        
        if(doTurn){
            int d_i_new = -d_j;
            int d_j_new = d_i;
            
            d_i = d_i_new;
            d_j = d_j_new;
        }
        k++;
    }
    
    return result;
}

@end
