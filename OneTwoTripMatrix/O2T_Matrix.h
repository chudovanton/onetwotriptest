//
//  O2T_Matrix.h
//  OneTwoTripMatrix
//
//  Created by Chudov Anton on 06/10/2016.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface O2T_Matrix : NSObject
-(instancetype) init2nMinus1Matrix:(int) n;
-(NSArray<NSNumber *>*) getSpiralSequence;
-(NSNumber *) matrixValueForIdx1: (int) idx1 Idx2:(int) idx2;
@end
