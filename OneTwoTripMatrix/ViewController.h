//
//  ViewController.h
//  OneTwoTripMatrix
//
//  Created by Chudov Anton on 06/10/2016.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>
;

- (IBAction)process:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *matrixDimText;
@property (weak, nonatomic) IBOutlet UITextView *matrixText;
@property (weak, nonatomic) IBOutlet UITextView *spiralSequenceText;

@end

