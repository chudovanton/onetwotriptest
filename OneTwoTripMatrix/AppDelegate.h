//
//  AppDelegate.h
//  OneTwoTripMatrix
//
//  Created by Chudov Anton on 06/10/2016.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

