//
//  ViewController.m
//  OneTwoTripMatrix
//
//  Created by Chudov Anton on 06/10/2016.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import "ViewController.h"
#import "O2T_Matrix.h"

@interface ViewController ()

@end

@implementation ViewController{
  
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _matrixDimText.delegate = self;
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)process:(id)sender {
    int n = [self.matrixDimText.text intValue];
    if(n > 0){
        O2T_Matrix *matrix  = [[O2T_Matrix alloc] init2nMinus1Matrix: n];
        
        
        self.matrixText.text = @"";
        
        for(int i= 0; i<(2*n-1);i++){
            NSString * matrixString = @"";
            for(int j =0; j< (2*n-1); j++){
                NSNumber *value = [matrix matrixValueForIdx1:i Idx2:j];
                matrixString = [matrixString stringByAppendingFormat:@" %@", [value stringValue]];
            }
            matrixString = [matrixString stringByAppendingString:@"\n"];
        
            self.matrixText.text = [self.matrixText.text stringByAppendingString: matrixString];
        }
        
        self.spiralSequenceText.text = @"";
        NSArray *spiral = [matrix getSpiralSequence];
        for(int i = 0;i<spiral.count;i++){
            NSNumber *value = spiral[i];
            self.spiralSequenceText.text = [self.spiralSequenceText.text stringByAppendingFormat:@"%@ ", [value stringValue]];
        }
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}

@end
